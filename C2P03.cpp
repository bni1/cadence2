#include "Supp.h"
#include "DLList.h"

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char **argv
* Post-conditions: int
* This is the main driver function that creates the DLList 
* and prints out node date in order 
********************************************/
int main(int argc, char **argv){
		std::ifstream inputStream;

		getFileStream(inputStream, argv[1]);

		DLList<int> theDLList;
addNodes(inputStream, theDLList);
		std::cout << theDLList << std::endl;

    return 0;
}
