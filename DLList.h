/**********************************************
* File: DLList.h
* Author: Jorge Garcia, Imari McKinney, Bo Ni, Reilley Knott
* Email: jgarci22@nd.edu, imckinne@nd.edu, bni@nd.edu, rknott@nd.edu
* This file is the declaration of and the implementation of the DLList class
**********************************************/
#ifndef DLLIST_H
#define DLLIST_H
#include <stdexcept>
#include "DLLNode.h"

template<class T>
class DLList{

	public:
 		Node<T> *head;
		Node<T> *tail;

/********************************************
* Function Name  : DLList<T>
* Pre-conditions : none
* Post-conditions: none
* This method creates the head node
********************************************/
DLList<T>()
{
	// new Node() creates a Node will a nullptr
	this->head = new Node<T>();
}

friend std::ostream& operator<< (std::ostream& stream, const DLList<T>& theList){
				Node<T>* temp;
				if (theList.head == NULL)
				{
								return stream;
				}
				temp = theList.head;
				while (temp != NULL)
				{
								stream << temp->data << " " ;
								temp = temp->next;
				}
				return stream;
}

/********************************************
* Function Name  : ~DLList<T>
* Pre-conditions : none
* Post-conditions: none
* This is a destructor method
********************************************/
~DLList<T>()
{
	Node<T>* current = head; // (1)

	while (current != nullptr) {
		Node<T>* next = current->next; // (2)
		delete current; // (3)
		current = next; // (4)
	}

head = nullptr;
}

/********************************************
* Function Name  : insert
* Pre-conditions : T value
* Post-conditions: none
* This method inserts values into node
********************************************/
void insert(T value){
	if (head->next == NULL) {
		//head = new Node<T>();
		tail = head; // Case 1: (1)
		head->next = tail; // Case 1: (2)
		head->data = value; // Case 1: (3)
	}

else {
		tail->next = new Node<T>(); // Case 2: (1)
		tail->next->prev = tail; // Case 2: (2)
		tail = tail->next; // Case 2: (3)
		tail->data = value; // Case 2: (4)
		tail->next = nullptr; // Case 2: (5)
	}
}

/********************************************
* Function Name  : deleteNode
* Pre-conditions : T key
* Post-conditions: none
* This method deletes a node
********************************************/
void deleteNode(T key){
		if(head == nullptr)
	throw std::out_of_range("invalid LinkedList Node");

else if(head->data == key){ // Case 2
	head = head->next; // Case 2 : (1)

	if(head != nullptr) // Case 2 : (2)
 	head->prev = nullptr;
 	return;

}

 Node<T>* current = head;
 Node<T>* previous = nullptr;

while(current != nullptr && current->data != key) {
		previous = current;
 		current = current->next;
 }
 
 //delete cur node
 previous->next = current->next; // Case 3: (1)
 
if(previous->next != nullptr) {
 previous->next->prev = previous; // Case 3: (2)
}

if(current == tail) {
	tail = nullptr;
	previous->next = tail; // Case 3: (3)
 	tail = previous; // Case 4: (4)
}

delete current;
}

};

#endif
