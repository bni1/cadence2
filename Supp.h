/**********************************************
* File: Supp.h
* Author: Jorge Garcia, Imari McKinney, Bo Ni, Reilley Knott
* Email: jgarci22@nd.edu, imckinne@nd.edu, bni@nd.edu, rknott@nd.edu
* Contains fuctions for the DLList.h file 
**********************************************/
#ifndef SUPP_H
#define SUPP_H
#include <iostream>
#include <fstream>
#include "DLList.h"
void getFileStream(std::ifstream& stream, char* fileName);
void addNodes(std::ifstream& inputStream, Node<int>* node1, Node<int>* node2, Node<int>* node3);
void addNodes(std::ifstream& inputStream, DLList<int>& theList);
void setPtrs(Node<int>*, Node<int>*, Node<int>*);
void printPtrs(std::ostream&, Node<int>*);
#endif  
